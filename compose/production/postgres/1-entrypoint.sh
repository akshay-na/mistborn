#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

DATA_DIR="/var/lib/postgresql/data"

RUNNING_VERSION="$(psql -V | awk -F' ' '{print $3}' | awk -F'.' '{print $1}')"
#DATA_VERSION="$(cat ${DATA_DIR}/PG_VERSION 2>/dev/null)"
BACKUP_FILE="$(ls -t /backups/ 2>/dev/null | head -n 1)"

# # case 1: first time setup
# # empty data folder
# if [[ -z "$DATA_VERSION" ]]; then
#     # let run normally, will initialize new environment
#     echo "First run detected. Starting normally..."

# # case 2: same major version
# elif [[ "$RUNNING_VERSION" == "$DATA_VERSION" ]]; then
#     # same major version, no intervention needed
#     echo "Same major version of PostgreSQL. Starting normally..."

# case 3: data dir is old, recent backup sql.gz exists
if [[ ! -z "$BACKUP_FILE" ]]; then
    echo "Checking $BACKUP_FILE"

    tempfile=$(mktemp /tmp/tempfile.XXXXXXXXXXXX)
    gunzip -c "/backups/$BACKUP_FILE" > "$tempfile"
    version="$(grep -oP "Dumped from database version .*" "$tempfile" | awk -F' ' '{print $5}' | awk -F'.' '{print $1}')"

    if [[ "$version" != "$RUNNING_VERSION" ]]; then
        echo "Dump is a different PostgreSQL version (${version}) than currently running (${RUNNING_VERSION})"
        echo "Restoring database from $BACKUP_FILE"
        #rm -rf /var/lib/postgresql/data/*
        restore $BACKUP_FILE
    else
        echo "Dump is the same version (${version}) as currently running PostgreSQL. Ignoring."
    fi

    rm $tempfile
fi


# case 4: data dir is old, backup sql.gz does NOT exist


exec "$@"